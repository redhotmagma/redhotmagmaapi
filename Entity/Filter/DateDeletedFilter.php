<?php

namespace Redhotmagma\ApiBundle\Entity\Filter;

use Doctrine\ORM;

/**
 * Class DateDeletedFilter
 *
 * Filters out deleted Entries
 * This Filter adds this constraint
 *
 * @package Redhotmagma\ConfiguratorApiBundle\Entity\Filter
 * @author  Michael Aichele <aichele@redhotmagma.de>
 *
 * @since   1.0
 * @version 1.0
 *
 * (c) redhotmagma
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
class DateDeletedFilter extends ORM\Query\Filter\SQLFilter
{

    const FILTER_NAME = 'datedeleted';


    /**
     * Called by DoctrineConnector to add the Filter to its configuration
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     * @since   1.0
     * @version 1.0
     *
     * @param   \Doctrine\ORM\Configuration $objOrmConfiguration
     */
    public static function addToConfiguration(ORM\Configuration $objOrmConfiguration)
    {

        $objOrmConfiguration->addFilter(self::FILTER_NAME, get_class());
    }

    /**
     * Called by Doctrine as soon as Filter is used
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     * @since   1.0
     * @version 1.0
     *
     * @param   \Doctrine\ORM\Mapping\ClassMetadata $targetEntity
     * @param   string                              $targetTableAlias
     *
     * @return  string
     */
    public function addFilterConstraint(ORM\Mapping\ClassMetadata $targetEntity, $targetTableAlias)
    {

        $strReturn = '';

        // If Entity has Field client_id and current Client is configured: Filter
        if ($targetEntity->hasField('date_deleted')) {
            $strReturn = $targetTableAlias . '.date_deleted = "0001-01-01 00:00:00"';
        }

        return $strReturn;
    }
}
