<?php

namespace Redhotmagma\ApiBundle\Test;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * offers common functions for API testing
 *
 * @author  Michael Aichele <aichele@redhotmagma.de>
 * @since   1.0
 * @version 1.0
 *
 * @package Tests\ConfiguratorApiBundle
 */
class ApiTestCase extends WebTestCase
{

    protected $em;


    /**
     * setup test, get entitymanager
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     * @since   1.0
     * @version 1.0
     */
    protected function setUp()
    {

        self::bootKernel();

        $this->em = static::$kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    /**
     * Create a client with a default Authorization header.
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     * @since   1.0
     * @version 1.0
     *
     * @param string $username
     * @param string $password
     *
     * @return \Symfony\Bundle\FrameworkBundle\Client
     */
    protected function createAuthenticatedClient($username = 'admin', $password = 'admin')
    {

        $client = static::createClient();
        $client->insulate();

        $client->request('POST', '/api/login_check', array(
            '_username' => $username,
            '_password' => $password,
        ));

        $response = $client->getResponse();

        $data = json_decode($response->getContent(), true);

        $client = static::createClient();
        $client->setServerParameter('HTTP_Authorization', sprintf('Bearer %s', $data['token']));

        return $client;
    }

    /**
     * get json data from response object and compare to excpected data
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     * @since   1.0
     * @version 1.0
     *
     * @param   Response $response
     * @param   string   $expected
     */
    protected function compareResponseContent($response, $expected)
    {

        $expectedJson = json_decode($expected);
        $responseJson = json_decode($response->getContent());

        $this->assertEquals($expectedJson, $responseJson, 'JSON content mismatch');
    }
}
