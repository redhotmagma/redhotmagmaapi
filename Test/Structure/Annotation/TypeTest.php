<?php

namespace Redhotmagma\ApiBundle\Test\Structure\Annotation;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ApiBundle\Structure\Annotation\Type;

class TypeTest extends TestCase
{
    public function testShouldResolveNormalClassAnnotation()
    {
        $className = '\Redhotmagma\ApiBundle\Test\Service\ChildStructure';
        $type = new Type(['value' => $className]);

        $this->assertFalse($type->isArray);
        $this->assertSame($className, $type->className);
        $this->assertSame($className, $type->value);
    }

    /**
     * @dataProvider providerResolveArrayClassAnnotation
     */
    public function testShouldResolveArrayClassAnnotation($annotation)
    {
        $className = '\Redhotmagma\ApiBundle\Test\Service\ChildStructure';
        $type = new Type(['value' => $annotation]);

        $this->assertTrue($type->isArray);
        $this->assertSame($className, $type->className);
        $this->assertSame($annotation, $type->value);
    }

    public function providerResolveArrayClassAnnotation()
    {
        return [
            ['\Redhotmagma\ApiBundle\Test\Service\ChildStructure[]'],
            ['array<\Redhotmagma\ApiBundle\Test\Service\ChildStructure>'],
        ];
    }
}
