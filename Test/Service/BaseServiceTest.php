<?php

namespace Redhotmagma\ApiBundle\Test\Service;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromDataConverter;
use Redhotmagma\ApiBundle\Structure\Annotation\Type;

class BaseServiceTest extends TestCase
{
    public function testShouldGetStructureFromData()
    {
        $parentStructure = new BaseServiceTestParentStructure();
        $parentStructure->someProperty = 'I am groot';

        $child = new BaseServiceTestChildStructure();
        $child->someChildProperty = 'Child groot';
        $parentStructure->somePropertyWithSingleChild = $child;
        $parentStructure->somePropertyWithSingleChildBachelor = $child;

        $parentStructure->somePropertyWithChildCollection = [];
        $child = new BaseServiceTestChildStructure();
        $child->someChildProperty = 'first child groot';
        $parentStructure->somePropertyWithChildCollection[] = $child;
        $parentStructure->somePropertyWithChildCollectionBachelor[] = $child;
        $child = new BaseServiceTestChildStructure();
        $child->someChildProperty = 'second child groot';
        $parentStructure->somePropertyWithChildCollection[] = $child;
        $parentStructure->somePropertyWithChildCollectionBachelor[] = $child;

        $data = json_decode(json_encode($parentStructure));

        // this is only to register the Type annotation for this test
        new \ReflectionClass(Type::class);

        $actual = (new StructureFromDataConverter())->convert($data, BaseServiceTestParentStructure::class);

        $parentMetaData = new \stdClass();

        $expected = new BaseServiceTestParentStructure();
        $expected->someProperty = 'I am groot';
        $parentMetaData->changedProperties = [
            'someProperty',
            'somePropertyWithSingleChild',
            'somePropertyWithChildCollection',
            'somePropertyWithSingleChildBachelor',
            'somePropertyWithChildCollectionBachelor',
        ];
        $expected->_metadata = $parentMetaData;

        $childMetaData = new \stdClass();
        $childMetaData->changedProperties = ['someChildProperty'];

        $child = new BaseServiceTestChildStructure();
        $child->someChildProperty = 'Child groot';
        $child->_metadata = $childMetaData;
        $expected->somePropertyWithSingleChild = $child;
        $withoutType = json_decode(json_encode($child));
        unset($withoutType->_metadata);
        $expected->somePropertyWithSingleChildBachelor = $withoutType;

        $expected->somePropertyWithChildCollection = [];
        $child = new BaseServiceTestChildStructure();
        $child->someChildProperty = 'first child groot';
        $child->_metadata = $childMetaData;
        $expected->somePropertyWithChildCollection[] = $child;
        $withoutType = json_decode(json_encode($child));
        unset($withoutType->_metadata);
        $expected->somePropertyWithChildCollectionBachelor[] = $withoutType;
        $child = new BaseServiceTestChildStructure();
        $child->someChildProperty = 'second child groot';
        $child->_metadata = $childMetaData;
        $expected->somePropertyWithChildCollection[] = $child;
        $withoutType = json_decode(json_encode($child));
        unset($withoutType->_metadata);
        $expected->somePropertyWithChildCollectionBachelor[] = $withoutType;

        $this->assertEquals($expected, $actual);
    }
}

#[\AllowDynamicProperties]
class BaseServiceTestParentStructure
{
    public $someProperty;

    /**
     * @Type("\Redhotmagma\ApiBundle\Test\Service\BaseServiceTestChildStructure")
     */
    public $somePropertyWithSingleChild;

    /**
     * @Type("array<\Redhotmagma\ApiBundle\Test\Service\BaseServiceTestChildStructure>")
     */
    public $somePropertyWithChildCollection;

    public $somePropertyWithSingleChildBachelor;

    public $somePropertyWithChildCollectionBachelor;
}

#[\AllowDynamicProperties]
class BaseServiceTestChildStructure
{
    public $someChildProperty;
}
