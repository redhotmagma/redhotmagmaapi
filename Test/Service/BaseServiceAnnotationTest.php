<?php

namespace Redhotmagma\ApiBundle\Test\Service;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromDataConverter;
use Redhotmagma\ApiBundle\Structure\Annotation\Type;

class BaseServiceAnnotationTest extends TestCase
{
    public function testShouldGetStructureFromData()
    {
        $parentStructure = new BaseServiceAnnotationTestParentStructure();
        $parentStructure->someProperty = 'I am groot';

        $child = new BaseServiceAnnotationTestChildStructure();
        $child->someChildProperty = 'Child groot';
        $parentStructure->somePropertyWithSingleChild = $child;

        $parentStructure->somePropertyWithChildCollection = [];
        $child = new BaseServiceAnnotationTestChildStructure();
        $child->someChildProperty = 'first child groot';
        $parentStructure->somePropertyWithChildCollection[] = $child;
        $child = new BaseServiceAnnotationTestChildStructure();
        $child->someChildProperty = 'second child groot';
        $parentStructure->somePropertyWithChildCollection[] = $child;

        $data = json_decode(json_encode($parentStructure));

        // this is only to register the Type annotation for this test
        new \ReflectionClass(Type::class);

        $actual = (new StructureFromDataConverter())->convert($data, BaseServiceAnnotationTestParentStructure::class);

        $parentMetaData = new \stdClass();

        $expected = new BaseServiceAnnotationTestParentStructure();
        $expected->someProperty = 'I am groot';
        $parentMetaData->changedProperties = ['someProperty', 'somePropertyWithSingleChild', 'somePropertyWithChildCollection'];
        $expected->_metadata = $parentMetaData;

        $childMetaData = new \stdClass();
        $childMetaData->changedProperties = ['someChildProperty'];

        $child = new BaseServiceAnnotationTestChildStructure();
        $child->someChildProperty = 'Child groot';
        $child->_metadata = $childMetaData;
        $expected->somePropertyWithSingleChild = $child;

        $expected->somePropertyWithChildCollection = [];
        $child = new BaseServiceAnnotationTestChildStructure();
        $child->someChildProperty = 'first child groot';
        $child->_metadata = $childMetaData;
        $expected->somePropertyWithChildCollection[] = $child;
        $child = new BaseServiceAnnotationTestChildStructure();
        $child->someChildProperty = 'second child groot';
        $child->_metadata = $childMetaData;
        $expected->somePropertyWithChildCollection[] = $child;

        $this->assertEquals($expected, $actual);
    }
}

#[\AllowDynamicProperties]
class BaseServiceAnnotationTestParentStructure
{
    public $someProperty;

    /**
     * @Type("\Redhotmagma\ApiBundle\Test\Service\BaseServiceAnnotationTestChildStructure")
     */
    public $somePropertyWithSingleChild;

    /**
     * @Type("\Redhotmagma\ApiBundle\Test\Service\BaseServiceAnnotationTestChildStructure[]")
     */
    public $somePropertyWithChildCollection;
}

#[\AllowDynamicProperties]
class BaseServiceAnnotationTestChildStructure
{
    public $someChildProperty;
}
