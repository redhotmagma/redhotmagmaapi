<?php

namespace Redhotmagma\ApiBundle\Test\Repository;

use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Setup;
use PHPUnit\Framework\TestCase;
use Redhotmagma\ApiBundle\Repository\Repository;

class RepositoryTest extends TestCase
{

    /**
     * @var EntityManager|\Phake_IMock
     * @Mock EntityManager
     */
    private $entityManager;

    /**
     * @var QueryBuilder|\Phake_IMock
     * @Mock QueryBuilder
     */
    private $queryBuilder;

    /**
     * @var AbstractQuery|\Phake_IMock
     * @Mock AbstractQuery
     */
    private $query;

    public function setUp(): void
    {
        $this->entityManager = \Phake::mock(EntityManager::class);
        $this->queryBuilder = \Phake::mock(QueryBuilder::class);
        $this->query = \Phake::mock(AbstractQuery::class);

        \Phake::when($this->entityManager)->createQueryBuilder()->thenReturn($this->queryBuilder);
        \Phake::when($this->queryBuilder)->select->thenReturn($this->queryBuilder);
        \Phake::when($this->queryBuilder)->from->thenReturn($this->queryBuilder);
        \Phake::when($this->queryBuilder)->getQuery->thenReturn($this->query);
    }

    public function testShouldBuildValidQueryForSingleQueryParameter()
    {
        $entityManager = EntityManager::create(['driver' => 'pdo_mysql'], Setup::createAnnotationMetadataConfiguration([], true));

        $searchablefields = ['field1', 'field2'];
        $parameters = ['query' => 'value1'];

        $repository = new Repository($entityManager, new ClassMetadata('SomeClass'));
        $reflection = new \ReflectionClass($repository);
        $method = $reflection->getMethod('addSearchToQuery');
        $method->setAccessible(true);

        $qb = new QueryBuilder($entityManager);
        $actual = $method->invokeArgs($repository, [$qb, [], $searchablefields, $parameters]);

        $this->assertEquals('someclass.field1 LIKE :_query', $actual[0]->getParts()[0]->__toString());
        $this->assertEquals('someclass.field2 LIKE :_query', $actual[0]->getParts()[1]->__toString());

        $parameters = $qb->getQuery()->getParameters()->toArray();
        $this->assertEquals('_query', $parameters[0]->getName());
        $this->assertEquals('%value1%', $parameters[0]->getValue());
    }

    public function testShouldBuildValidQueryForMultipleQueryParameter()
    {
        $entityManager = EntityManager::create(['driver' => 'pdo_mysql'], Setup::createAnnotationMetadataConfiguration([], true));

        $searchablefields = ['field1', 'field2'];
        $parameters = ['query' => 'value1,value2,value3'];

        $repository = new Repository($entityManager, new ClassMetadata('SomeClass'));
        $reflection = new \ReflectionClass($repository);
        $method = $reflection->getMethod('addSearchToQuery');
        $method->setAccessible(true);

        $qb = new QueryBuilder($entityManager);
        $actual = $method->invokeArgs($repository, [$qb, [], $searchablefields, $parameters]);

        $this->assertEquals('someclass.field1 LIKE :_query', $actual[0]->getParts()[0]->__toString());
        $this->assertEquals('someclass.field1 LIKE :_query_1', $actual[0]->getParts()[1]->__toString());
        $this->assertEquals('someclass.field1 LIKE :_query_2', $actual[0]->getParts()[2]->__toString());
        $this->assertEquals('someclass.field2 LIKE :_query', $actual[0]->getParts()[3]->__toString());
        $this->assertEquals('someclass.field2 LIKE :_query_1', $actual[0]->getParts()[4]->__toString());
        $this->assertEquals('someclass.field2 LIKE :_query_2', $actual[0]->getParts()[5]->__toString());

        $parameters = $qb->getQuery()->getParameters()->toArray();
        $this->assertEquals('_query', $parameters[0]->getName());
        $this->assertEquals('%value1%', $parameters[0]->getValue());
        $this->assertEquals('_query_1', $parameters[1]->getName());
        $this->assertEquals('%value2%', $parameters[1]->getValue());
        $this->assertEquals('_query_2', $parameters[2]->getName());
        $this->assertEquals('%value3%', $parameters[2]->getValue());
    }

    public function testShouldSetLimitOffsetInt()
    {

        $searchablefields = ['field1', 'field2'];
        $parameters = ['limit' => '10', 'offset' => '15'];

        $repository = new Repository($this->entityManager, new ClassMetadata('SomeClass'));
        $repository->fetchList($parameters, $searchablefields);

        \Phake::verify($this->queryBuilder)->setFirstResult(15);
        \Phake::verify($this->queryBuilder)->setMaxResults(10);
    }
}
