<?php declare(strict_types=1);

namespace Redhotmagma\ApiBundle\Structure\Annotation;

/**
 * @Annotation
 */
class Type
{
    /**
     * @var string
     */
    public $value;

    /**
     * @var string
     */
    public $className;

    /**
     * @var bool
     */
    public $isArray = false;

    public function __construct($options)
    {
        $this->value = $options['value'];
        $this->className = $options['value'];

        $this->setup();
    }

    private function setup()
    {
        $patterns = [
            '/^array<(.*)>$/',
            '/^(.*)\[\]$/',
        ];
        foreach ($patterns as $pattern) {
            preg_match($pattern, $this->value, $matches);
            if (isset($matches[1])) {
                $this->className = $matches[1];
                $this->isArray = true;
                break;
            }
        }
    }
}
