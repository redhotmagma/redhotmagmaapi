<?php

namespace Redhotmagma\ApiBundle\Structure\Listresult;

class ListResult
{

    /**
     * @var Metadata
     */
    public $metadata;

    /**
     * @var array
     */
    public $data;
}
