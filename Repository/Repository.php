<?php

namespace Redhotmagma\ApiBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\QueryBuilder;

/**
 * Class Repository
 *
 * Mother of all Configurator Repositories, inherits a lot of Grandma Doctrine\ORM\EntityRepository
 *
 * @package Redhotmagma\ConfiguratorApiBundle\Repository
 * @author  Christian Schilling <schilling@redhotmagma.de>
 *
 * @since   1.0
 * @version 1.0
 *
 * (c) redhotmagma
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
class Repository extends EntityRepository
{

    /**
     * @var string
     */
    private $entityClassName;

    /**
     * @var string
     */
    private $normalizedEntityName;


    /**
     * @return mixed
     */
    public function getEntityClassName()
    {

        return $this->entityClassName;
    }


    /**
     * @param mixed $entityClassName
     */
    public function setEntityClassName($entityClassName)
    {

        $this->entityClassName = $entityClassName;
    }


    /**
     * @return string
     */
    public function getNormalizedEntityName()
    {

        return $this->normalizedEntityName;
    }


    /**
     * @param string $normalizedEntityName
     */
    public function setNormalizedEntityName($normalizedEntityName)
    {

        $this->normalizedEntityName = $normalizedEntityName;
    }


    /**
     * get classname and normalized entity name fronm repositorys entity
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     * @since   1.0
     * @version 1.0
     */
    public function __construct($em, ClassMetadata $class)
    {

        parent::__construct($em, $class);

        $this->setEntityClassName($class->getName());
        $arrEntityName = explode('\\', $this->entityClassName);
        $strEntityname = array_pop($arrEntityName);
        $this->setNormalizedEntityName(strtolower($strEntityname));
    }


    /**
     * save an entity
     *
     * @param mixed $entity
     * @param bool $flush
     *
     * @return mixed
     * @author  Michael Aichele <aichele@redhotmagma.de>
     * @since   1.0
     * @version 1.0
     *
     */
    public function save($entity, $flush = true)
    {
        $this->getEntityManager()->persist($entity);

        if ($flush === true) {
            $this->getEntityManager()->flush();
        }

        return $entity;
    }


    /**
     * deletes an entity
     *
     * @param mixed $entity
     * @param bool $flush
     * @version 1.0
     *
     * @author  Christian Schilling <schilling@redhotmagma.de>
     * @since   1.0
     */
    public function delete($entity, $flush = true)
    {

        $objDate = new \DateTime('now');

        $entity->setDateDeleted($objDate);

        $this->getEntityManager()
            ->persist($entity);

        if ($flush === true) {
            $this->getEntityManager()
                ->flush();
        }
    }


    /**
     * flush persisted entities to db
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     * @since   1.0
     * @version 1.0
     */
    public function flush()
    {

        $this->getEntityManager()
            ->flush();
    }


    /**
     * fetch a list of entities based on parameters
     *
     * @param mixed $parameters
     * @param array $searchablefields
     *
     * @return  array
     * @author  Michael Aichele <aichele@redhotmagma.de>
     * @since   1.0
     * @version 1.0
     *
     */
    public function fetchList($parameters, $searchablefields)
    {

        $query = $this->buildQueryFromParameters($parameters, $searchablefields);

        $result = $query->getResult();

        return $result;
    }


    /**
     * fetch the total count of a list based on parameters
     *
     * @param mixed $parameters
     * @param array $searchablefields
     *
     * @return  integer
     * @author  Michael Aichele <aichele@redhotmagma.de>
     * @since   1.0
     * @version 1.0
     *
     */
    public function fetchListCount($parameters, $searchablefields)
    {

        $query = $this->buildQueryFromParameters($parameters, $searchablefields, true);

        $result = $query->getSingleScalarResult();

        return $result;
    }


    /**
     * reconnect entity manager
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     * @since   1.0
     * @version 1.0.8
     */
    public function reconnect()
    {

        $this->getEntityManager()->getConnection()->close();
        $this->getEntityManager()->getConnection()->connect();
    }


    /**
     * Starts a transaction
     *
     * @param int $transactionTimeout
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     * @since   1.0
     * @version 1.0.7
     *
     * @author  Christian Schilling <schilling@redhotmagma.de>
     */
    public function beginTransaction($transactionTimeout = null)
    {

        if ($transactionTimeout !== null) {

            $sql = 'SET innodb_lock_wait_timeout = ' . (int)$transactionTimeout . ';';
            $query = $this->getEntityManager()->getConnection()->prepare($sql);
            $query->execute();
        }

        $this->getEntityManager()->beginTransaction();
    }


    /**
     * commits a started transaction
     *
     * @author  Christian Schilling <schilling@redhotmagma.de>
     * @since   1.0
     * @version 1.0
     *
     */
    public function commitTransaction()
    {

        $this->getEntityManager()->commit();
    }


    /**
     * Executes plain DQL - be careful with that!
     *
     * @param string $dql
     * @return mixed
     * @version 1.0
     *
     * @author  Christian Schilling <schilling@redhotmagma.de>
     * @since   1.0
     */
    public function executeDQL($dql)
    {

        $query = $this->getEntityManager()->createQuery($dql);

        $result = $query->execute();

        return $result;
    }


    /**
     * Wrapper for em->clear()
     *
     * @author  Christian Schilling <schilling@redhotmagma.de>
     * @since   1.0
     * @version 1.0
     *
     */
    public function clear()
    {
        $this->getEntityManager()->clear();
    }


    /**
     * Return the count for a given queryBuilder
     *
     * @param QueryBuilder $queryBuilder
     *
     * @return integer
     * @author Christian Kuhnle
     * @since  2017-08
     *
     */
    public function getCountForQueryBuilder(QueryBuilder $queryBuilder)
    {
        $queryBuilder->select('COUNT( DISTINCT ' . $this->getNormalizedEntityName() . '.id)');
        $count = $queryBuilder->getQuery()->getSingleScalarResult();

        return $count;
    }


    /**
     * build query from parameters
     *
     * @param array $parameters
     * @param array $searchablefields
     * @param boolean $iscountquery
     *
     * @return \Doctrine\ORM\Query
     * @since   1.0
     * @version 1.0
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     */
    protected function buildQueryFromParameters($parameters, $searchablefields, $iscountquery = false)
    {
        $queryBuilder = $this->getQueryBuilderFromParameters($parameters, $searchablefields, $iscountquery);

        $query = $queryBuilder->getQuery();

        return $query;
    }


    /**
     * get querybuilder from parameters
     * seperated from buildQueryFromParameters to be able to add stuff to the querybuilder
     *
     * @param array $parameters
     * @param array $searchablefields
     * @param boolean $iscountquery
     *
     * @return \Doctrine\ORM\QueryBuilder
     * @since   1.0
     * @version 1.0
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     */
    protected function getQueryBuilderFromParameters($parameters, $searchablefields, $iscountquery = false)
    {

        if (!is_array($parameters)) {
            $parameters = $parameters->toArray();
        }

        $entityname = $this->getNormalizedEntityName();
        $querybuilder = $this->createQueryBuilder($entityname);

        if ($iscountquery === true) {
            $querybuilder->select('COUNT( DISTINCT ' . $entityname . '.id)');
        }

        $queryparts = array();
        $queryparts = $this->addSearchToQuery($querybuilder, $queryparts, $searchablefields, $parameters);
        $queryparts = $this->addFiltersToQuery($querybuilder, $queryparts, $parameters);

        if (!empty($queryparts)) {
            $where = call_user_func_array(array($querybuilder->expr(), 'andX'), $queryparts);
            $querybuilder->where($where);
        }

        if ($iscountquery === false) {
            if (!empty($parameters['orderby'])) {

                // auto joins for ordering
                $orderFieldParts = explode('.', $parameters['orderby']);

                if (sizeof($orderFieldParts) > 1) {

                    $parameters['orderby'] = $orderFieldParts[count($orderFieldParts) - 2] . '.' . $orderFieldParts[count($orderFieldParts) - 1];
                }

                foreach ($orderFieldParts as $i => $filterfieldPart) {

                    if ($i > 0 && $i < count($orderFieldParts) - 1) {

                        if (!in_array($orderFieldParts[$i], $querybuilder->getAllAliases())) {

                            $querybuilder->leftJoin($orderFieldParts[$i - 1] . '.' . $orderFieldParts[$i],
                                $orderFieldParts[$i]);
                        }
                    }
                }

                $orderdirection = 'ASC';
                if (!empty($parameters['orderdir']) && strtolower($parameters['orderdir']) === 'desc') {
                    $orderdirection = 'DESC';
                }

                $querybuilder->orderBy($parameters['orderby'], $orderdirection);
            }

            if (isset($parameters['limit']) && isset($parameters['offset'])) {
                $querybuilder->setFirstResult((int)$parameters['offset']);
                $querybuilder->setMaxResults((int)$parameters['limit']);
            }

            $querybuilder->groupBy($this->getNormalizedEntityName() . '.id');
        }

        return $querybuilder;
    }


    /**
     * build search statement part adding all searchfilds as or conditions
     *
     * @param QueryBuilder $querybuilder
     * @param array $queryparts
     * @param array $searchablefields
     * @param array $parameters
     *
     * @return  array
     * @version 1.0
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     * @since   1.0
     */
    protected function addSearchToQuery($querybuilder, $queryparts, $searchablefields, $parameters)
    {
        $entityname = $this->getNormalizedEntityName();
        $joins = array();

        if (!empty($parameters['query'])) {
            if (!empty($searchablefields)) {
                $searchExp = array();
                $splitQuery = explode(',', $parameters['query']);

                foreach ($searchablefields as $searchablefield) {
                    // add dynamic joins
                    $joinlist = explode('.', $searchablefield);
                    if (count($joinlist) > 1) {
                        for ($i = 0; $i < count($joinlist) - 1; $i++) {
                            if (!isset($joins[$entityname . '.' . $joinlist[$i]])) {
                                $querybuilder->leftJoin($entityname . '.' . $joinlist[$i], $joinlist[$i]);

                                $joins[$entityname . '.' . $joinlist[$i]] = $entityname . '.' . $joinlist[$i];
                            }
                        }

                        foreach ($splitQuery as $key => $value) {
                            $queryBindParam = ':_query' . ($key > 0 ? '_' . $key : '');
                            $searchExp[] = $querybuilder->expr()->like($searchablefield, $queryBindParam);
                        }
                    } else {
                        foreach ($splitQuery as $key => $value) {
                            $queryBindParam = ':_query' . ($key > 0 ? '_' . $key : '');
                            $searchExp[] = $querybuilder->expr()->like($entityname . '.' . $searchablefield, $queryBindParam);
                        }
                    }
                }

                $queryparts[] = call_user_func_array(array($querybuilder->expr(), 'orX'), $searchExp);

                foreach ($splitQuery as $key => $value) {
                    $queryParamKey = ':_query' . ($key > 0 ? '_' . $key : '');
                    $querybuilder->setParameter($queryParamKey, '%' . $value . '%');
                }
            }
        }

        return $queryparts;
    }


    /**
     * build filter statement part adding all filters from params as and conditions
     *
     * @param QueryBuilder $querybuilder
     * @param array $queryparts
     * @param array $parameters
     *
     * @return  array
     * @since   1.0
     * @version 1.0
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     */
    protected function addFiltersToQuery($querybuilder, $queryparts, $parameters)
    {

        if (!empty($parameters['filters'])) {
            $expressions = array();
            foreach ($parameters['filters'] as $filterfield => $filtervalue) {
                $placeholder = str_replace('.', '_', $filterfield);

                $filterfieldParts = explode('.', $filterfield);

                foreach ($filterfieldParts as $i => $filterfieldPart) {

                    if ($i > 0 && $i < count($filterfieldParts) - 1) {

                        if (!in_array($filterfieldParts[$i], $querybuilder->getAllAliases())) {

                            $querybuilder->join($filterfieldParts[$i - 1] . '.' . $filterfieldParts[$i],
                                $filterfieldParts[$i]);
                        }
                    }
                }

                $filterfieldForQuery = $filterfield;
                if (count($filterfieldParts) > 2) {

                    $filterfieldForQuery = $filterfieldParts[count($filterfieldParts) - 2] . '.' . $filterfieldParts[count($filterfieldParts) - 1];
                }

                // if the filter value is exactly "{{null}}" filter wit h isNull, else perform a like search
                if ($filtervalue === '{{null}}') {

                    $expressions[] = $querybuilder->expr()
                        ->isNull($filterfieldForQuery);
                } else {

                    $expressions[] = $querybuilder->expr()
                        ->like($filterfieldForQuery, ':' . $placeholder);

                    if ($filterfieldParts[count($filterfieldParts) - 1] == 'id') {

                        $querybuilder->setParameter($placeholder, $filtervalue);
                    } else {

                        $querybuilder->setParameter($placeholder, '%' . $filtervalue . '%');
                    }
                }
            }

            $queryparts[] = call_user_func_array(array($querybuilder->expr(), 'andX'), $expressions);
        }

        return $queryparts;
    }
}
