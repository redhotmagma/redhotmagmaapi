<?php

namespace Redhotmagma\ApiBundle\Command;

use Doctrine\Bundle\DoctrineBundle\Command\Proxy;
use Doctrine\ORM\Tools\SchemaTool;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class UpdateSchemaCommand
 *
 * CoreBundles own extension of php app/console doctrine:schema:update --force
 *
 * Does exactly the same thing but removes Foreign Keys from the Schema
 * The actual removal happens in the derived SchemaTool CustomSchemaTool
 *
 * php app/console redhotmagmacore:updateschema --force
 *
 * @package Redhotmagma\ConfiguratorApiBundle\Command
 * @author  Christian Schilling <schilling@redhotmagma.de>
 *
 * @since   1.0
 * @version 1.0
 *
 * (c) redhotmagma
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
class UpdateSchemaCommand extends Proxy\UpdateSchemaDoctrineCommand
{

    /**
     * Overwrites configure(), changes name and alters Description
     *
     * @author  Christian Schilling <schilling@redhotmagma.de>
     * @since   1.0
     * @version 1.0
     */
    protected function configure()
    {

        parent::configure();

        $this
            ->setName('redhotmagmacore:updateschema')
            ->setDescription('Executes (or dumps) the SQL needed to update the database schema to match the current mapping metadata.' .
                ' Overwritten to not add FKs');
    }

    /**
     * {@inheritdoc}
     *
     * Overwritten to use Configurator's own SchemaTool
     *
     * @author  Christian Schilling <schilling@redhotmagma.de>
     * @since   1.0
     * @version 1.0
     */
    protected function executeSchemaCommand(
        InputInterface $input,
        OutputInterface $output,
        SchemaTool $schemaTool,
        array $metadatas,
        SymfonyStyle $ui
    ) {

        // use own derived Schema Tool that removes FKs
        $objEntityManager = $this->getHelper('em')->getEntityManager();

        $schemaTool = new CustomSchemaTool($objEntityManager);

        return parent::executeSchemaCommand($input, $output, $schemaTool, $metadatas, $ui);
    }
}
