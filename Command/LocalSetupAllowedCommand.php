<?php

namespace Redhotmagma\ApiBundle\Command;

use Doctrine\DBAL\Connection;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class LocalSetupAllowedCommand extends ContainerAwareCommand
{
   /**
     * @author  Andreas Nölke <noelke@redhotmagma.de>
     * @since   1.0
     * @version 1.0
     *
     * @inheritdoc
     */
    protected function configure()
    {
        $this
            ->setName('redhotmagmacore:check-local-setup-allowed')
            ->setDescription('Check if the local Setup is allowed.')
            ->setHelp('This command helps you to check if the local setup is allowed. It checks for a special unittest DB. ' .
                'If this DB does not exist, the check will return false.');
    }

    /**
     * @author  Andreas Nölke <noelke@redhotmagma.de>
     * @since   1.0
     * @version 1.0
     *
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Connection $connection */
        $connection = $this->getContainer()->get('doctrine')->getConnection();
        $result = $connection->fetchAll('SELECT SCHEMA_NAME FROM information_schema.SCHEMATA s WHERE s.SCHEMA_NAME = \'this_is_the_unit_testing_dbms\';');

        if (empty($result)) {
            $statusCode = 1;
            $output->writeln('This is <error>not</error> a valid local environment!');
            $output->writeln('The databse \'this_is_the_unit_testing_dbms\' could not be found!');
        } else {
            $statusCode = 0;
            $output->writeln('This is a <info>valid</info> local environment!');
        }

        return (int)$statusCode;
    }
}
