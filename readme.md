# redhotmagma API bundle

Provides a couple of base classes and mechanisms to create a symfony based api.
Mainly transfers plain structures to doctrine entities and the other way round.

## Installation

Import config File in app/config.yml
```
imports:
- { resource: "@RedhotmagmaApiBundle/Resources/config/config.yml" }
```
Add Bundle to AppKernel.php
```
$bundles = [
    ...
    new Redhotmagma\ApiBundle\RedhotmagmaApiBundle(),
    ...
];
```

## Run tests
Docker compose is needed to run the tests.

`docker-compose run --rm app vendor/bin/phpunit`

## Structure usage sample

You can create structures wherever you want.
If you have child structures use the `Type` annotation to automatically resolve them. Be sure to use the 
`Redhotmagma\ApiBundle\Structure\Annotation\Type` namespace in your structure class.

```php
namespace YourBundle\Structrue;

use Redhotmagma\ApiBundle\Structure\Annotation\Type;

class ParentStructure
{
    public $someProperty;

    /**
     * @Type("\YourBundle\Structrue\ChildStructure")
     */
    public $somePropertyWithSingleChild;

    /**
     * @Type("\YourBundle\Structrue\ChildStructure[]")
     */
    public $somePropertyWithChildCollection;
}

class ChildStructure
{
    public $someChildProperty;
}
```

http://www.redhotmagma.de
