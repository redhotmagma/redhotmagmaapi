<?php

namespace Redhotmagma\ApiBundle\Controller;

use Doctrine\ORM\EntityRepository;

use Redhotmagma\ApiBundle\Service\DefaultApiServices\DeleteService;
use Redhotmagma\ApiBundle\Service\DefaultApiServices\FetchListService;
use Redhotmagma\ApiBundle\Service\DefaultApiServices\FetchOneService;
use Redhotmagma\ApiBundle\Service\DefaultApiServices\GetResponseService;
use Redhotmagma\ApiBundle\Service\DefaultApiServices\SaveService;
use Redhotmagma\ApiBundle\Service\ServiceInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class Controller
 *
 * Base Controller of the API. Provides helper functions to read and write data.
 *
 * @package Redhotmagma\ConfiguratorApiBundle\Controller
 * @author  Christian Schilling <schilling@redhotmagma.de>
 *
 * @since   1.0
 * @version 1.0
 */
abstract class Controller extends \Symfony\Bundle\FrameworkBundle\Controller\Controller
{

    /**
     * @var GetResponseService
     */
    private $getResponseService;

    /**
     * @var FetchListService
     */
    private $fetchListService;

    /**
     * @var FetchOneService
     */
    private $fetchOneService;

    /**
     * @var DeleteService
     */
    private $deleteService;

    /**
     * @var SaveService
     */
    private $saveService;


    /**
     * @return GetResponseService
     */
    public function getGetResponseService()
    {
        return $this->getResponseService;
    }


    /**
     * @param GetResponseService $getResponseService
     */
    public function setGetResponseService($getResponseService)
    {
        $this->getResponseService = $getResponseService;
    }


    /**
     * @return FetchListService
     */
    public function getFetchListService()
    {
        return $this->fetchListService;
    }


    /**
     * @param FetchListService $fetchListService
     */
    public function setFetchListService($fetchListService)
    {
        $this->fetchListService = $fetchListService;
    }


    /**
     * @return FetchOneService
     */
    public function getFetchOneService()
    {
        return $this->fetchOneService;
    }


    /**
     * @param FetchOneService $fetchOneService
     */
    public function setFetchOneService($fetchOneService)
    {
        $this->fetchOneService = $fetchOneService;
    }


    /**
     * @return DeleteService
     */
    public function getDeleteService()
    {
        return $this->deleteService;
    }


    /**
     * @param DeleteService $deleteService
     */
    public function setDeleteService($deleteService)
    {
        $this->deleteService = $deleteService;
    }


    /**
     * @return SaveService
     */
    public function getSaveService()
    {
        return $this->saveService;
    }


    /**
     * @param SaveService $saveService
     */
    public function setSaveService($saveService)
    {
        $this->saveService = $saveService;
    }


    /**
     * Returns a Response object with JSON data for a given dataset (object, structure, array)
     * Encodes data via json_encode
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     * @since   1.0
     * @version 1.0
     *
     * @param   array $listdata
     * @param   integer $count
     * @param   string $responsestructurename
     *
     * @return Response
     */
    protected function getListJsonResponse($listdata, $count, $responsestructurename)
    {

        $response = $this->getGetResponseService()->getListJsonResponse($listdata, $count, $responsestructurename);

        return $response;
    }


    /**
     * create and return list metadata object with given totalcount
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     * @since   1.0
     * @version 1.0
     *
     * @param   integer $count
     *
     * @return  Metadata
     */
    protected function getListMetadata($count)
    {

        $metadata = $this->getGetResponseService()->getListMetadata($count);

        return $metadata;
    }


    /**
     * Returns a Response object with JSON data for a given dataset (object, structure, array)
     * Encodes data via json_encode
     *
     * @author  Christian Schilling <schilling@redhotmagma.de>
     * @since   1.0
     * @version 1.0
     *
     * @param array|object $data
     * @param boolean $allowempty
     *
     * @return Response
     */
    protected function getJsonResponse($data = null, $allowempty = false)
    {

        $response = $this->getGetResponseService()->getJsonResponse($data, $allowempty);

        return $response;
    }


    /**
     * Returns a Response object with JSON data for validation errors
     * Encodes data via json_encode
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     * @since   1.0
     * @version 1.0
     *
     * @param array|object $errors
     *
     * @return Response
     */
    protected function getValidationErrorResponse($errors)
    {
        $data = new \stdClass();
        $data->errors = $errors;
        $data->success = false;
        $data->code = 'validation_error';

        $json = json_encode($data);

        $response = new Response($json, 400, array(
            'Content-Type' => 'application/json'
        ));

        return $response;
    }


    /**
     * get list parameters from request. query, limits, order
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     * @since   1.0
     * @version 1.0
     *
     * @param   Request $request
     *
     * @return  array
     */
    protected function getListParameters($request)
    {
        $parameters = array();

        $parameters['query'] = $request->get('query');
        $parameters['filters'] = $request->get('filters', array());

        $offset = $request->get('offset');
        if($offset !== null) {

            $parameters['offset'] = $offset;
        }

        $limit = $request->get('limit');
        if($limit !== null) {

            $parameters['limit'] = $limit;
        }

        $parameters['orderby'] = $request->get('orderby');
        $parameters['orderdir'] = $request->get('orderdir', 'ASC');

        return $parameters;
    }


    /**
     * Fetches a list of datasets based on a given repository, its corresponding service (or global service)
     * and an optional list of parameters.
     *
     * Returns an array of structures.
     *
     * @author  Christian Schilling <schilling@redhotmagma.de>
     * @since   1.0
     * @version 1.0
     *
     * @param EntityRepository $repository
     * @param ServiceInterface $service
     * @param array $parameters
     *
     * @return mixed
     */
    protected function fetchList($repository, $service, $parameters = array())
    {

        $structures = $this->getFetchListService()->fetchList($repository, $service, $parameters,
            $this->getSearchableFields());

        return $structures;
    }


    /**
     * Fetches one dataset based on a given id, repository, its corresponding service (or global service)
     *
     * Returns one structure.
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     * @since   1.0
     * @version 1.0
     *
     * @param   EntityRepository $repository
     * @param   ServiceInterface $service
     * @param   integer $id
     *
     * @return  mixed
     */
    protected function fetchOne($repository, $service, $id)
    {

        $structure = $this->getFetchOneService()->fetchOne($repository, $service, $id);

        return $structure;
    }


    /**
     * Fetches a list by conditions
     * "Alias" for doctrine findBy
     *
     * @author  Christian Schilling <schilling@redhotmagma.de>
     * @since   1.0
     * @version 1.0
     *
     * @param $repository
     * @param $service
     * @param $conditions
     *
     * @return mixed
     */
    protected function fetchBy($repository, $service, $conditions)
    {
        $data = $repository->findBy($conditions);

        $structures = $service->getStructuresFromEntities($data);

        return $structures;
    }


    /**
     * deletes datasets based on a given id(s), repository, its corresponding service (or global service)
     *
     * Returns one structure.
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     * @since   1.0
     * @version 1.0
     *
     * @param   EntityRepository $repository
     * @param   string $ids
     */
    protected function delete($repository, $ids)
    {

        $this->getDeleteService()->delete($repository, $ids);
    }


    /**
     * deletes one dataset based on a given id, repository, its corresponding service (or global service)
     *
     * Returns one structure.
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     * @since   1.0
     * @version 1.0
     *
     * @param   EntityRepository $repository
     * @param   integer $id
     *
     * @return  mixed
     */
    protected function deleteOne($repository, $id)
    {

        $entity = $this->getDeleteService()->deleteOne($repository, $id);

        return $entity;
    }


    /**
     * saves an entity and returns it
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     * @since   1.0
     * @version 1.0
     *
     * @param   EntityRepository $repository
     * @param   ServiceInterface $service
     * @param   object $structure
     * @param   string $entityclassname
     *
     * @return  mixed
     */
    protected function save($repository, $service, $structure, $entityclassname = null)
    {
        $entity = $this->getSaveService()->save($repository, $service, $structure, $entityclassname);

        return $entity;
    }


    /**
     * a list af fields that are searchable with the query param
     *
     * @author  Christian Schilling <schilling@redhotmagma.de>
     * @since   1.0
     * @version 1.0
     *
     * @return  array
     */
    protected function getSearchableFields()
    {

        return array();
    }

}
