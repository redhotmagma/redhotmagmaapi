<?php

namespace Redhotmagma\ApiBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;

class RedhotmagmaApiExtension extends Extension implements PrependExtensionInterface
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $this->processConfiguration($configuration, $configs);

        $loader = new YamlFileLoader(
            $container,
            new FileLocator(__DIR__ . '/../Resources/config')
        );

        $loader->load('services.yml');
    }

    /**
     * @param ContainerBuilder $container
     */
    public function prepend(ContainerBuilder $container)
    {
        try {
            $configs = Yaml::parseFile(__DIR__ . '/../Resources/config/override.yml');
        } catch (ParseException $exception) {
            throw new InvalidArgumentException(
                sprintf(
                    'The file override.yml of this Bundle does not contain valid YAML: %s',
                    $exception->getMessage()
                ),
                0,
                $exception
            );
        }

        foreach ($configs as $configKey => $config) {
            $container->prependExtensionConfig($configKey, $config);
        }
    }
}
