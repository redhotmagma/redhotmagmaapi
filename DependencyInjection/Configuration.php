<?php

namespace Redhotmagma\ApiBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    private const BUNDLE_KEY = 'redhotmagma_api';

    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        if (method_exists(TreeBuilder::class, 'root')) {
            $treebuilder = new TreeBuilder();
            $treebuilder->root(self::BUNDLE_KEY);

            return $treebuilder;
        }

        return new TreeBuilder(self::BUNDLE_KEY);
    }
}
