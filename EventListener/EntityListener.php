<?php

namespace Redhotmagma\ApiBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class EntityListener
 *
 * @package Redhotmagma\ConfiguratorApiBundle\EventListener
 * @author  Michael Aichele <aichele@redhotmagma.de>
 *
 * @since   1.0
 * @version 1.0
 *
 * (c) redhotmagma
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
class EntityListener
{
    const DATE_DELETED_DEFAULT = '0001-01-01 00:00:00';

    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    /**
     * To not cause any compatibility issues $dateDeletedDefault should stay.
     * @var string
     */
    protected $dateDeleteDefault = self::DATE_DELETED_DEFAULT;


    /**
     * Constructor
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     * @since   1.0
     * @version 1.0
     *
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(TokenStorageInterface $tokenStorage)
    {

        $this->tokenStorage = $tokenStorage;
    }

    /**
     * preUpdate
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     * @since   1.0
     * @version 1.0
     *
     * @param LifecycleEventArgs $args
     */
    public function preUpdate(LifecycleEventArgs $args)
    {

        $this->updateEntity($args);
    }

    /**
     * prePersist
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     * @since   1.0
     * @version 1.0
     *
     * @param LifecycleEventArgs $args
     */
    public function prePersist(LifecycleEventArgs $args)
    {

        $this->updateEntity($args);
    }

    /**
     * updates the entities default fields
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     * @since   1.0
     * @version 1.0
     *
     * @param LifecycleEventArgs $args
     */
    protected function updateEntity($args)
    {

        $entity = $args->getObject();
        $intId = $entity->getId();

        // set User - can be null (anonymous)
        $intUserId = 0;
        if ($this->tokenStorage->getToken()) {
            $objUser = $this->tokenStorage->getToken()->getUser();
        }

        if (!empty($objUser) && is_object($objUser)) {
            $intUserId = $objUser->getId();
        }

        if (empty($intId)) {
            if (method_exists($entity, 'setDateCreated')) {
                $dateCreated = $entity->getDateCreated();
                if (empty($dateCreated)) {
                    $entity->setDateCreated(new \DateTime());
                }
            }
            if (method_exists($entity, 'setDateDeleted')) {
                $entity->setDateDeleted(new \DateTime($this->dateDeleteDefault));
            }
            if (method_exists($entity, 'setUserCreatedId')) {
                $entity->setUserCreatedId($intUserId);
            }
        } else {
            if (method_exists($entity, 'getDateDeleted')
                && method_exists($entity, 'setUserDeletedId')
                && $entity->getDateDeleted()->format('Y-m-d H:i:s') > $this->dateDeleteDefault
            ) {
                $entity->setUserDeletedId($intUserId);
            } else {
                $entity->setDateUpdated(new \DateTime());
                $entity->setUserUpdatedId($intUserId);
            }
        }
    }
}
