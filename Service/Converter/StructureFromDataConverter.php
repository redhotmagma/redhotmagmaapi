<?php

namespace Redhotmagma\ApiBundle\Service\Converter;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\SimpleAnnotationReader;
use Redhotmagma\ApiBundle\Service\Helper\StringHelper;
use Redhotmagma\ApiBundle\Structure\Annotation\Type;

class StructureFromDataConverter implements StructureFromDataConverterInterface
{


    /**
     * fill a structures fields from data (decoded json) recursively
     *
     * @param   \stdClass $data
     * @param   string $structureclassname
     *
     * @return  mixed
     */
    public function convert($data, $structureclassname)
    {
        // return data of a stdClass directly since it is no structure that can be converted
        if($structureclassname === \stdClass::class) {
            return $data;
        }

        $structure = new $structureclassname();
        $reflection = new \ReflectionClass(get_class($structure));

        $simpleAnnotationReader = new AnnotationReader();

        $metadata = new \stdClass();
        $metadata->changedProperties = array();

        foreach ($reflection->getProperties() as $property) {
            $propertyName = $property->getName();

            if (property_exists($data, $propertyName)) {
                $metadata->changedProperties[] = $propertyName;
                $structure->$propertyName = $data->$propertyName;

                $typeAnnotation = $this->getPropertyAnnotation($simpleAnnotationReader, $property);

                if (!is_null($typeAnnotation)) {
                    if ($typeAnnotation->isArray) {
                        $propertyValues = $data->$propertyName;

                        if (!empty($propertyValues)) {
                            $properties = array();
                            foreach ($propertyValues as $propertykey => $propertydata) {

                                if (class_exists($typeAnnotation->className)) {
                                    // if the propertydata is not an object assume that we got a list of ids
                                    // transform to a stdclass with this id then
                                    if (!is_object($propertydata)) {
                                        $propertyId = $propertydata;
                                        $propertydata = new \stdClass();
                                        $propertydata->id = $propertyId;
                                    }

                                    $properties[$propertykey] = $this->convert($propertydata,
                                        $typeAnnotation->className);
                                } else {
                                    $properties[$propertykey] = $propertydata;
                                }
                            }

                            $structure->$propertyName = $properties;
                        }
                    } else {
                        if (class_exists($typeAnnotation->className)) {

                            if (is_object($data->$propertyName) || is_array(is_object($data->$propertyName))) {
                                $structure->$propertyName =
                                    $this->convert($data->$propertyName, $typeAnnotation->className);
                            } else {
                                if ($data->$propertyName !== null) {

                                    $propertydata = new $typeAnnotation->className();
                                    $propertydata->id = $structure->$propertyName;
                                    $structure->$propertyName = $propertydata;
                                }
                            }
                        }
                    }
                }
            }
        }

        $structure->_metadata = $metadata;

        return $structure;
    }


    /**
     * @param AnnotationReader $simpleAnnotationReader
     * @param \ReflectionProperty $property
     *
     * @return null|Type
     */
    private function getPropertyAnnotation(
        AnnotationReader $simpleAnnotationReader,
        \ReflectionProperty $property
    ): Type {
        /** @var Type $typeAnnotation */
        $typeAnnotation = $simpleAnnotationReader->getPropertyAnnotation($property, Type::class);
        $annotation = $typeAnnotation->value ?? null;

        if (is_null($annotation)) {
            $doc = $property->getDocComment();
            preg_match('/@.*Type\(\"(.*?)\"\)/s', $doc, $annotations);
            $annotation = $annotations[1] ?? null;
            if (!is_null($annotation)) {
                $typeAnnotation = new Type(['value' => $annotation]);
            }
        }

        if (is_null($typeAnnotation)) {
            $typeAnnotation = new Type(['value' => '']);
        }

        return $typeAnnotation;
    }

}
