<?php

namespace Redhotmagma\ApiBundle\Service\Converter;

use Redhotmagma\ApiBundle\Service\Helper\StringHelper;

class EntityHelper implements EntityHelperInterface
{

    /**
     * @var StringHelper
     */
    protected $stringHelper;



    public function __construct(
        StringHelper $stringHelper
    ) {
        $this->stringHelper = $stringHelper;
    }


    /**
     * if the name of the target entity ist not set ($entityclassname) the class name is set to the structures name
     * if no entity class is found default to stdclass
     *
     * @param   object $structure
     * @param   string $entityclassname
     *
     * @return  object|\stdClass
     */
    public function getEntityClass($structure, $entityClassName = null)
    {
        if ($structure !== null && $entityClassName === null) {
            $class = new \ReflectionClass(get_class($structure));
            $entityClassName = str_replace('\\Structure\\', '\\Entity\\', $class->name);
        }

        if ($entityClassName !== null && class_exists($entityClassName)) {
            $entity = new $entityClassName();
        } else {
            $entity = new \stdClass();
        }

        return $entity;
    }


    /**
     * executes the setter for the given property
     *
     * @param   object $entity
     * @param   string $property
     * @param   mixed $value
     *
     * @return  object
     */
    public function updateEntityProperty($entity, $property, $value)
    {

        $setter = 'set' . ucfirst($this->stringHelper->underscoresToCamelCase($property));

        if (method_exists($entity, $setter)) {

            $entity->$setter($value);
        }

        return $entity;
    }
}
