<?php
namespace Redhotmagma\ApiBundle\Service\Converter;

use Redhotmagma\ApiBundle\Service\Helper\StringHelper;

class StructureFromEntityConverter implements StructureFromEntityConverterInterface
{

    /**
     * @var StringHelper
     */
    private $stringHelper;

    /**
     * @var StructureHelper
     */
    private $structureHelper;


    public function __construct(
        StringHelper $stringHelper,
        StructureHelper $structureHelper)
    {
        $this->stringHelper = $stringHelper;
        $this->structureHelper = $structureHelper;
    }


    /**
     * transforms an entity to a structure for the api
     *
     * @param   object $entity
     * @param   string $structureclassname (including namespace)
     *
     * @return  mixed
     */
    public function convertOne($entity, $structureclassname = null)
    {

        // return null for deleted entities
        if ($entity->getDateDeleted()
                ->format('Y-m-d') == '0001-01-01'
        ) {
            $structure = $this->structureHelper->getStructureClass($entity, $structureclassname);

            // iterate over structure properties, if entity has an getter for that property get data from there
            foreach ($structure as $property => $value) {
                $getter = 'get' . ucfirst($this->stringHelper->underscoresToCamelCase($property));

                if (method_exists($entity, $getter)) {

                    $propertyValue = $entity->$getter();

                    // convert the property value to int if the getter should return an int but the value is string
                    // this is needed for bigint fields in the database that are handled as strings by doctrine
                    $reflection = new \ReflectionClass(get_class($entity));

                    // if the reflected class is a doctrine proxy get the reflection of the real class instead
                    if(strpos($reflection->name, 'Proxies\__CG__') === 0) {
                        $reflection = new \ReflectionClass(str_replace('Proxies\__CG__', '', $reflection->name));
                    }

                    $docBlock = $reflection->getMethod($getter)->getDocComment();
                    if (strpos($docBlock, '@return int') !== false && is_string($propertyValue)) {

                        $propertyValue = (int)$propertyValue;
                    }

                    $structure->$property = $propertyValue;
                }
            }
        } else {
            $structure = null;
        }

        return $structure;
    }



    /**
     * transforms an array of entities to structures
     *
     * @param array  $entities
     * @param string $structureclassname (including namespace)
     *
     * @return array
     */
    public function convertMany($entities, $structureclassname = null)
    {

        $structures = array();

        foreach ($entities as $entity) {
            $structure = $this->convertOne($entity, $structureclassname);
            $structures[] = $structure;
        }

        return $structures;
    }

    /**
     *
     * Completes a RelationStructure by Data out of the relations Entity
     *
     * @param $relationStructure
     * @param $relation
     * @param array $lockedOriginProperties
     * @return mixed
     */
    public function completeByRelationData(
        $relationStructure,
        $relation,
        array $lockedOriginProperties = []
    ) {
        foreach ((array)$relationStructure as $key => $data) {
            $propertyGetter = 'get' . ucfirst($this->stringHelper->underscoresToCamelCase($key));

            /*
             * if property exists in target and relation data will be retrieved
             * - by default from relation
             * - skipped if excluded by $lockedOriginProperties
             */
            if (method_exists($relation, $propertyGetter) &&
                $relation->$propertyGetter() !== null &&
                !in_array($key, $lockedOriginProperties))  {
                $relationStructure->$key = $relation->$propertyGetter();
            }
        }
        return $relationStructure;
    }
}
