<?php
namespace Redhotmagma\ApiBundle\Service\Converter;


interface EntityHelperInterface
{

    public function getEntityClass($structure, $entityclassname = null);

    public function updateEntityProperty($entity, $property, $value);
}
