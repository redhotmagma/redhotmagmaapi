<?php
namespace Redhotmagma\ApiBundle\Service\Converter;

interface StructureFromEntityConverterInterface
{
    public function convertOne($entity, $structureclassname);

    public function convertMany($entities, $structureclassname);
}
