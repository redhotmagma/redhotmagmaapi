<?php
namespace Redhotmagma\ApiBundle\Service\Converter;

class StructureHelper
{

    /**
     * if the name of the target structure ist not set ($structureclassname) the class name is set to the entities name
     * if no structure class is found default to stdclass
     *
     * @param   object $entity
     * @param   string $structureclassname
     *
     * @return object|\stdClass
     */
    public function getStructureClass($entity, $structureclassname = null)
    {
        if (empty($structureclassname) && !empty($entity)) {

            $class = new \ReflectionClass(get_class($entity));

            $structureclassname = str_replace('\\Entity\\', '\\Structure\\', $class->name);
            $structureclassname = str_replace('Proxies\\__CG__\\', '', $structureclassname);
        }

        if (class_exists($structureclassname)) {
            $structure = new $structureclassname();
        } else {
            $structure = new \stdClass();
        }

        return $structure;
    }

}
