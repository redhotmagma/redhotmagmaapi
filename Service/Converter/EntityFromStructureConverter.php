<?php

namespace Redhotmagma\ApiBundle\Service\Converter;

class EntityFromStructureConverter implements EntityFromStructureConverterInterface
{

    /**
     * @var EntityHelper
     */
    private $entityHelper;


    public function __construct(EntityHelper $entityHelper)
    {
        $this->entityHelper = $entityHelper;
    }


    public function convertOne($structure, $entity = null, $entityclassname = null)
    {

        if (empty($entity)) {
            $entity = $this->entityHelper->getEntityClass($structure, $entityclassname);
        }

        $changedProperties = array();
        if (isset($structure->_metadata) && !empty($structure->_metadata->changedProperties)) {
            $changedProperties = $structure->_metadata->changedProperties;
        }

        foreach ($structure as $property => $value) {
            if (empty($changedProperties) || in_array($property, $changedProperties)) {

                $entity = $this->entityHelper->updateEntityProperty($entity, $property, $value);
            }
        }

        return $entity;
    }


    /**
     * mark all relations in entity that are not existent in structure as deleted
     *
     * @param   object $structure
     * @param   mixed $entity
     * @param   string $relationname
     * @param   string $relationsourcename
     * @param   string $relationtargetname
     *
     * @return  mixed
     */
    public function setManyToManyRelationsDeleted(
        $structure,
        $entity,
        $relationname,
        $relationsourcename,
        $relationtargetname
    ) {

        $relationgetter = 'get' . $relationname;
        $relationtargetgetter = 'get' . $relationtargetname;

        $relations = $entity->$relationgetter();
        if (!empty($relations)) {
            foreach ($entity->$relationgetter() as $relationentity) {
                $found = false;
                if (isset($structure->$relationsourcename)) {
                    foreach ($structure->$relationsourcename as $relationstructure) {
                        if ($relationentity->$relationtargetgetter()
                                ->getId() == $relationstructure->id
                        ) {
                            $found = true;
                        }
                    }
                }

                if (!$found) {
                    $relationentity->setDateDeleted(new \DateTime());
                }
            }
        }

        return $entity;
    }


    /**
     * add all relations from structure that are not existent in entity as new relation
     *
     * @param   object $structure
     * @param   mixed $entity
     * @param   string $relationclassname
     * @param   string $relationsourcename
     * @param   string $relationtargetname
     * @param   mixed $relationtargetrepository
     *
     * @return  mixed
     */
    public function addNewManyToManyRelations(
        $structure,
        $entity,
        $relationclassname,
        $relationsourcename,
        $relationtargetname,
        $relationtargetrepository
    ) {

        $classparts = explode('\\', $relationclassname);
        $relationname = $classparts[count($classparts) - 1];

        $relationgetter = 'get' . $relationname;
        $relationtargetgetter = 'get' . $relationtargetname;

        $relationadder = 'add' . $relationname;
        $relationtargetsetter = 'set' . $relationtargetname;

        $sourceclass = new \ReflectionClass(get_class($entity));
        $classparts = explode('\\', $sourceclass->name);
        $relationsourcesetter = 'set' . $classparts[count($classparts) - 1];

        if (isset($structure->$relationsourcename)) {
            foreach ($structure->$relationsourcename as $relationstructure) {
                $found = false;
                $relations = $entity->$relationgetter();
                if (!empty($relations)) {
                    foreach ($entity->$relationgetter() as $relationentity) {
                        if ($relationentity->$relationtargetgetter()
                                ->getId() == $relationstructure->id
                        ) {
                            $found = true;

                            break;
                        }
                    }
                }

                if (!$found) {
                    $targetentity = $relationtargetrepository->findOneBy(array('id' => $relationstructure->id));

                    $objNewRelation = new $relationclassname();
                    $objNewRelation->$relationtargetsetter($targetentity);
                    $objNewRelation->$relationsourcesetter($entity);

                    $entity->$relationadder($objNewRelation);
                }
            }
        }

        return $entity;
    }


    /**
     * mark all relations in entity that are not existent in structure as deleted
     *
     * @param   mixed $structure
     * @param   mixed $entity
     * @param   string $relationstructurename name of the relation in the structure
     * @param   string $relationentityname name of the relation in the entity
     *
     * @return  mixed
     */
    public function setManyToOneRelationsDeleted($structure, $entity, $relationstructurename, $relationentityname)
    {

        $relationentitygetter = 'get' . $relationentityname;
        $relationentities = $entity->$relationentitygetter();

        if (!empty($relationentities)) {
            foreach ($relationentities as $relation) {
                $found = false;

                // check if relation is still in structure
                if (!empty($structure->$relationstructurename)) {
                    foreach ($structure->$relationstructurename as $structurerelation) {
                        if ($structurerelation->id == $relation->getId()) {
                            $found = true;
                        }
                    }
                }

                if (!$found) {
                    $relation->setDateDeleted(new \DateTime());
                }
            }
        }

        return $entity;
    }


    /**
     * add all relations from structure that are not existent in entity as new relation
     *
     * @param   mixed $structure
     * @param   mixed $entity
     * @param   string $relationstructurename
     * @param   string $relationentityname
     *
     * @return  mixed
     */
    public function addNewManyToOneRelations($structure, $entity, $relationstructurename, $relationentityname)
    {
        $relationentitygetter = 'get' . $relationentityname;

        $relationentityadder = 'add' . $relationentityname;
        $relationentityremove = 'remove' . $relationentityname;

        $sourceclass = new \ReflectionClass(get_class($entity));
        $classparts = explode('\\', $sourceclass->name);
        $relationsourcesetter = 'set' . $classparts[count($classparts) - 1];
        $relationEntityName = implode('\\', array_slice($classparts, 0, -1)) . '\\' . $relationentityname;

        $relationentities = $entity->$relationentitygetter();

        if (!empty($structure->$relationstructurename)) {
            foreach ($structure->$relationstructurename as $structurerelation) {
                $found = false;
                if (!empty($relationentities)) {
                    foreach ($relationentities as $relation) {
                        if (!empty($structurerelation->id) && $structurerelation->id == $relation->getId()) {
                            $found = true;
                            $entity->$relationentityremove($relation);
                            break;
                        }
                    }
                }

                if (!$found) {
                    $newRelationEntity = $this->convertOne($structurerelation, null, $relationEntityName);
                    $newRelationEntity->$relationsourcesetter($entity);
                } else {
                    $newRelationEntity = $this->convertOne($structurerelation, $relation, $relationEntityName);
                }

                $entity->$relationentityadder($newRelationEntity);
            }
        }

        return $entity;
    }
}
