<?php
namespace Redhotmagma\ApiBundle\Service\Converter;

interface EntityFromStructureConverterInterface
{
    public function convertOne($structure, $entity, $entityclassname);
}
