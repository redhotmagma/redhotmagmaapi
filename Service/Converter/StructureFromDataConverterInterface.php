<?php
namespace Redhotmagma\ApiBundle\Service\Converter;

interface StructureFromDataConverterInterface
{

    public function convert($data, $structureclassname);

}
