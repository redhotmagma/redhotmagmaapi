<?php

namespace Redhotmagma\ApiBundle\Service\DefaultApiServices;


class FetchOneService
{

    /**
     * Fetches one dataset based on a given id, repository, its corresponding service (or global service)
     *
     * Returns one structure.
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     * @since   1.0
     * @version 1.0
     *
     * @param   EntityRepository $repository
     * @param   ServiceInterface $service
     * @param   integer $id
     *
     * @return mixed
     *
     * @throws Exception
     */
    public function fetchOne($repository, $service, $id)
    {
        $structure = null;

        if (empty($repository) || empty($service)) {
            throw new Exception('Repository or Service not set');
        }

        if (empty($id)) {
            throw new Exception('ID not set');
        }

        $data = $repository->findOneBy(array('id' => $id));
        if (!empty($data)) {
            $structure = $service->getStructureFromEntity($data);
        }
        return $structure;
    }
}
