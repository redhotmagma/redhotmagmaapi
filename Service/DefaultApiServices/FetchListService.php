<?php

namespace Redhotmagma\ApiBundle\Service\DefaultApiServices;


class FetchListService {

    /**
     * Fetches a list of datasets based on a given repository, its corresponding service (or global service)
     * and an optional list of parameters.
     *
     * Returns an array of structures.
     *
     * @param EntityRepository $repository
     * @param ServiceInterface $service
     * @param array $parameters
     * @param array $searchableFields
     *
     * @return array
     *
     * @throws Exception
     */
    public function fetchList($repository, $service, $parameters = array(), $searchableFields = array())
    {

        if(empty($repository) || empty($service)) {
            throw new Exception('Repository or Service not set');
        }

        $data = $repository->fetchList($parameters, $searchableFields);
        $structures = $service->getStructuresFromEntities($data);

        return $structures;
    }
}
