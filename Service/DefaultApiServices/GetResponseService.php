<?php
namespace Redhotmagma\ApiBundle\Service\DefaultApiServices;


use Symfony\Component\HttpFoundation\Response;

class GetResponseService
{

    /**
     * Returns a Response object with JSON data for a given dataset (object, structure, array)
     * Encodes data via json_encode
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     * @since   1.0
     * @version 1.0
     *
     * @param   array $listdata
     * @param   integer $count
     * @param   string $responsestructurename
     *
     * @return Response
     */
    public function getListJsonResponse($listdata, $count, $responsestructurename)
    {
        $data = new $responsestructurename();
        $data->data = $listdata;

        $data->metadata = $this->getListMetadata($count);

        $response = $this->getJsonResponse($data);

        return $response;
    }


    /**
     * Returns a Response object with JSON data for a given dataset (object, structure, array)
     * Encodes data via json_encode
     *
     * @author  Christian Schilling <schilling@redhotmagma.de>
     * @since   1.0
     * @version 1.0
     *
     * @param array|object $data
     * @param boolean $allowempty
     *
     * @return Response
     */
    public function getJsonResponse($data = null, $allowempty = false)
    {
        if (!empty($data) || $allowempty === true) {

            $responseContent = '';
            if ($data !== null) {

                $responseContent = json_encode($data);
            }
            $response = new Response($responseContent, 200, array(
                'Content-Type' => 'application/json'
            ));
        } else {
            $response = new Response('', 404, array(
                'Content-Type' => 'application/json'
            ));
        }

        return $response;
    }


    /**
     * create and return list metadata object with given totalcount
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     * @since   1.0
     * @version 1.0
     *
     * @param   integer $count
     *
     * @return  Metadata
     */
    public function getListMetadata($count)
    {
        $metadata = new \Redhotmagma\ApiBundle\Structure\Listresult\Metadata();
        $metadata->totalcount = $count;

        return $metadata;
    }
}