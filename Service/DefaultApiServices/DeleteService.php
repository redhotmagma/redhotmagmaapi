<?php

namespace Redhotmagma\ApiBundle\Service\DefaultApiServices;


class DeleteService
{

    /**
     * deletes datasets based on a given id(s), repository, its corresponding service (or global service)
     *
     * Returns one structure.
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     * @since   1.0
     * @version 1.0
     *
     * @param  EntityRepository $repository
     * @param  string $ids
     *
     * @return array
     *
     * @throws Exception
     */
    public function delete($repository, $ids)
    {
        $entities = array();

        if (empty($repository)) {
            throw new Exception('Repository or Service not set');
        }

        if (empty($ids)) {
            throw new Exception('IDs not set');
        }

        $ids = explode(',', $ids);
        foreach ($ids as $id) {
            $entities[] = $this->deleteOne($repository, trim($id));
        }


        return $entities;
    }


    /**
     * deletes one dataset based on a given id, repository, its corresponding service (or global service)
     *
     * Returns one structure.
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     * @since   1.0
     * @version 1.0
     *
     * @param   EntityRepository $repository
     * @param   integer $id
     * @return mixed
     * @throws Exception
     */
    public function deleteOne($repository, $id)
    {
        if (empty($repository)) {
            throw new Exception('Repository or Service not set');
        }

        if (empty($id)) {
            throw new Exception('ID not set');
        }

        $entity = $repository->findOneBy(array('id' => $id));

        if (!empty($entity)) {
            $repository->delete($entity);
        }

        return $entity;
    }
}
