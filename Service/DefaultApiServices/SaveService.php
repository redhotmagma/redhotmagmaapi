<?php

namespace Redhotmagma\ApiBundle\Service\DefaultApiServices;


class SaveService
{

    /**
     * saves an entity and returns it
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     * @since   1.0
     * @version 1.0
     *
     * @param   EntityRepository $repository
     * @param   ServiceInterface $service
     * @param   object $structure
     * @param   string $entityclassname
     * @return  mixed
     * @throws  Exception
     */
    public function save($repository, $service, $structure, $entityclassname = null)
    {

        if(empty($repository) || empty($service)) {
            throw new Exception('Repository or Service not set');
        }

        if(empty($structure)) {
            throw new Exception('Structure not set');
        }

        $entity = null;
        if (isset($structure->id) && $structure->id > 0) {
            $entity = $repository->findOneBy(array('id' => $structure->id));
        }

        $entity = $service->getEntityFromStructure($structure, $entity, $entityclassname);

        $repository->save($entity);

        return $entity;
    }
}
