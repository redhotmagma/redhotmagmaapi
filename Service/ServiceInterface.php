<?php

namespace Redhotmagma\ApiBundle\Service;

/**
 * Interface ServiceInterface
 *
 * @package Redhotmagma\ConfiguratorApiBundle\Service
 * @author  Christian Schilling <schilling@redhotmagma.de>
 *
 * @since   1.0
 * @version 1.0
 */
interface ServiceInterface
{

    /**
     * transforms an entity to a structure for the api
     *
     * @author  Christian Schilling <schilling@redhotmagma.de>
     * @since   1.0
     * @version 1.0
     *
     * @param   object $entity
     * @param   string $structureclassname (including namespace)
     *
     * @return  object|\stdClass
     */
    public function getStructureFromEntity($entity, $structureclassname = null);


    /**
     * transforms an array of entities to structures
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     * @since   1.0
     * @version 1.0
     *
     * @param array  $entities
     * @param string $structureclassname (including namespace)
     *
     * @return array
     */
    public function getStructuresFromEntities($entities, $structureclassname = null);


    /**
     * transforms a structure to an entity
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     * @since   1.0
     * @version 1.0
     *
     * @param   object $structure
     * @param   object $entity
     * @param   string $entityclassname (including namespace)
     *
     * @return  object|\stdClass
     */
    public function getEntityFromStructure($structure, $entity, $entityclassname);
}
