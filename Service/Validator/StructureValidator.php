<?php

namespace Redhotmagma\ApiBundle\Service\Validator;

use Symfony\Component\Validator\Validator\ValidatorInterface;

class StructureValidator implements StructureValidatorInterface
{

    /**
     * @var ValidatorInterface
     */
    private $validator;


    public function __construct(
        ValidatorInterface $validator
    ) {

        $this->validator = $validator;
    }

    /**
     * checks if the given structure needs to be validated
     * if only the id is set to be saved the structure needs no validation
     *
     * @param   mixed $structure
     *
     * @return  boolean
     */
    public function needsValidation($structure)
    {

        $needsValidation = true;

        if (isset($structure->_metadata)
            && isset($structure->_metadata->changedProperties)
            && is_countable($structure->_metadata->changedProperties)
            && count($structure->_metadata->changedProperties) == 1
            && $structure->_metadata->changedProperties[0] == 'id'
        ) {
            $needsValidation = false;
        }

        return $needsValidation;
    }

    /**
     * validate a structure based on its field annotations
     *
     * @param   mixed $structure
     *
     * @return  array $violations
     * @throws \Exception
     */
    public function validate($structure)
    {

        $errors = array();

        if (empty($this->validator)) {
            throw new \Exception('No validator given');
        }
        if (!empty($this->validator)) {

            $errors = $this->validator->validate($structure);
        }

        $violations = array();
        foreach ($errors as $violation) {
            $violations[] = array(
                'property' => $violation->getPropertyPath(),
                'invalidvalue' => $violation->getInvalidValue(),
                'message' => $violation->getMessage()
            );
        }

        return $violations;
    }

    /**
     * @param $subStructure
     * @param array $violations
     * @param string $marker
     * @param int|null $structureArrayKey
     * @return array
     * @throws \Exception
     */
    public function validateSubStructureOne(
        $subStructure,
        array $violations,
        string $marker,
        int $structureArrayKey = null
    ): array {
        $subViolations = $this->validate($subStructure);
        /** @var array $violation */
        foreach ($subViolations as $i => $violation) {
            if ($structureArrayKey !== null) {
                $subViolations[$i]['property'] = $marker . '[' . $structureArrayKey . '].' . $violation['property'];
            } else {
                $subViolations[$i]['property'] = $marker . '.' . $violation['property'];
            }
        }

        $violations = array_merge($violations, $subViolations);

        return $violations;
    }

    /**
     * @param array $subStructures
     * @param array $violations
     * @param string $marker
     * @return array
     * @throws \Exception
     */
    public function validateSubStructureMany(array $subStructures, array $violations, string $marker): array
    {
        /**  @var mixed $subStructure */
        foreach ($subStructures as $structureArrayKey => $subStructure) {
            $violations = $this->validateSubStructureOne($subStructure, $violations, $marker, $structureArrayKey);
        }

        return $violations;
    }
}
