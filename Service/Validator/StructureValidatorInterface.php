<?php

namespace Redhotmagma\ApiBundle\Service\Validator;

interface StructureValidatorInterface
{

    public function needsValidation($structure);
}
