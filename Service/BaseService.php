<?php

namespace Redhotmagma\ApiBundle\Service;

use Doctrine\Common\Annotations\SimpleAnnotationReader;
use Doctrine\ORM\Mapping\Entity;
use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverter;
use Redhotmagma\ApiBundle\Service\Converter\EntityHelper;
use Redhotmagma\ApiBundle\Service\Converter\EntityHelperInterface;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromDataConverter;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverterInterface;
use Redhotmagma\ApiBundle\Service\Converter\StructureHelper;
use Redhotmagma\ApiBundle\Service\Validator\StructureValidator;
use Redhotmagma\ApiBundle\Structure\Annotation\Type;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class BaseService
 *
 * parent class for services
 *
 * @author  Michael Aichele <aichele@redhotmagma.de>
 * @since   1.0
 * @version 1.0
 */
class BaseService implements ServiceInterface
{

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var StructureHelper
     */
    private $structureHelper;

    /**
     * @var EntityHelperInterface
     */
    private $entityHelper;

    /**
     * @var EntityFromStructureConverter
     */
    private $entityFromStructureConverter;

    /**
     * @var StructureFromEntityConverterInterface
     */
    private $structureFromEntityConverter;

    /**
     * @var StructureFromDataConverter
     */
    private $structureFromDataConverter;

    /**
     * @var StructureValidator
     */
    private $structureValidator;


    /**
     * @param ValidatorInterface $validator
     */
    public function setValidator($validator)
    {
        $this->validator = $validator;
    }


    /**
     * @return ValidatorInterface
     */
    public function getValidator()
    {
        return $this->validator;
    }


    public function __construct(
        StructureFromEntityConverterInterface $structureFromEntityConverter,
        StructureHelper $structureHelper,
        EntityHelperInterface $entityHelper,
        EntityFromStructureConverter $entityFromStructureConverter,
        StructureFromDataConverter $structureFromDataConverter,
        StructureValidator $structureValidator
    ) {

        $this->structureFromEntityConverter = $structureFromEntityConverter;
        $this->structureHelper = $structureHelper;
        $this->entityHelper = $entityHelper;
        $this->entityFromStructureConverter = $entityFromStructureConverter;
        $this->structureFromDataConverter = $structureFromDataConverter;
        $this->structureValidator = $structureValidator;
    }


    /**
     * transforms an entity to a structure for the api
     *
     * @param   object $entity
     * @param   string $structureclassname (including namespace)
     *
     * @return  mixed
     */
    public function getStructureFromEntity($entity, $structureclassname = null)
    {

        $structure = $this->structureFromEntityConverter->convertOne($entity, $structureclassname);

        return $structure;
    }


    /**
     * transforms an entity to a structure for the frontend, uses getStructureFromEntity, can be overwritten in specific services
     *
     * @param   object $entity
     * @param   string $structureclassname (including namespace)
     *
     * @return  mixed
     */
    public function getFrontendStructureFromEntity($entity, $structureclassname = null)
    {

        $structure = $this->getStructureFromEntity($entity, $structureclassname);

        return $structure;
    }


    /**
     * transforms an array of entities to structures
     *
     * @param array $entities
     * @param string $structureclassname (including namespace)
     *
     * @return array
     */
    public function getStructuresFromEntities($entities, $structureclassname = null)
    {

        $structures = array();

        foreach ($entities as $entity) {
            $structure = $this->getStructureFromEntity($entity, $structureclassname);
            $structures[] = $structure;
        }

        return $structures;
    }


    /**
     * transforms an array of entities to structures for frontend
     * @param array $entities
     * @param string $structureclassname (including namespace)
     *
     * @return array
     */
    public function getFrontendStructuresFromEntities($entities, $structureclassname = null)
    {

        $structures = array();

        foreach ($entities as $entity) {
            $structure = $this->getFrontendStructureFromEntity($entity, $structureclassname);
            $structures[] = $structure;
        }

        return $structures;
    }


    /**
     * transforms a structure to an entity
     * if the structure has changedProperties information, only update these properties in entity
     *
     * special handling for language fields (automatically get related language)
     *
     * @param   object $structure
     * @param   object $entity
     * @param   string $entityclassname (including namespace)
     *
     * @return  object|\stdClass
     */
    public function getEntityFromStructure($structure, $entity = null, $entityclassname = null)
    {

        $entity = $this->entityFromStructureConverter->convertOne($structure, $entity, $entityclassname);

        return $entity;
    }


    /**
     * fill a structures fields from data (decoded json) recursively
     *
     * @param   \stdClass $data
     * @param   string $structureclassname
     *
     * @return  mixed
     */
    public function getStructureFromData($data, $structureclassname)
    {

        $structure = $this->structureFromDataConverter->convert($data, $structureclassname);

        return $structure;
    }


    /**
     * mark all relations in entity that are not existent in structure as deleted
     *
     * @param   object $structure
     * @param   Entity $entity
     * @param   string $relationname
     * @param   string $relationsourcename
     * @param   string $relationtargetname
     *
     * @return  Entity
     */
    public function setManyToManyRelationsDeleted(
        $structure,
        $entity,
        $relationname,
        $relationsourcename,
        $relationtargetname
    ) {

        $this->entityFromStructureConverter->setManyToManyRelationsDeleted(
            $structure,
            $entity,
            $relationname,
            $relationsourcename,
            $relationtargetname
        );

        return $entity;
    }


    /**
     * add all relations from structure that are not existent in entity as new relation
     *
     * @param   object $structure
     * @param   Entity $entity
     * @param   string $relationclassname
     * @param   string $relationsourcename
     * @param   string $relationtargetname
     * @param   Repository $relationtargetrepository
     *
     * @return  Entity
     */
    public function addNewManyToManyRelations(
        $structure,
        $entity,
        $relationclassname,
        $relationsourcename,
        $relationtargetname,
        $relationtargetrepository
    ) {

        $entity = $this->entityFromStructureConverter->addNewManyToManyRelations(
            $structure,
            $entity,
            $relationclassname,
            $relationsourcename,
            $relationtargetname,
            $relationtargetrepository
        );

        return $entity;
    }


    /**
     * mark all relations in entity that are not existent in structure as deleted
     *
     * @param   mixed $structure
     * @param   mixed $entity
     * @param   string $relationstructurename name of the relation in the structure
     * @param   string $relationentityname name of the relation in the entity
     *
     * @return  mixed
     */
    public function setManyToOneRelationsDeleted($structure, $entity, $relationstructurename, $relationentityname)
    {

        $entity = $this->entityFromStructureConverter->setManyToOneRelationsDeleted(
            $structure,
            $entity,
            $relationstructurename,
            $relationentityname);

        return $entity;
    }


    /**
     * add all relations from structure that are not existent in entity as new relation
     *
     * @param   mixed $structure
     * @param   mixed $entity
     * @param   string $relationstructurename
     * @param   string $relationentityname
     *
     * @return  mixed
     */
    public function addNewManyToOneRelations($structure, $entity, $relationstructurename, $relationentityname)
    {

        $entity = $this->entityFromStructureConverter->addNewManyToOneRelations(
            $structure,
            $entity,
            $relationstructurename,
            $relationentityname
        );

        return $entity;
    }


    /**
     * if the name of the target structure ist not set ($structureclassname) the class name is set to the entities name
     * if no structure class is found default to stdclass
     *
     * @author  Michael Aichele <aichele@redhotmamga.de>
     * @since   1.0
     * @version 1.0
     *
     * @param   object $entity
     * @param   string $structureclassname
     *
     * @return object|\stdClass
     */
    protected function getStructureClass($entity, $structureclassname = null)
    {
        $structure = $this->structureHelper->getStructureClass($entity, $structureclassname);

        return $structure;
    }


    /**
     * if the name of the target entity ist not set ($entityclassname) the class name is set to the structures name
     * if no entity class is found default to stdclass
     *
     * @param   object $structure
     * @param   string $entityclassname
     *
     * @return  object|\stdClass
     */
    protected function getEntityClass($structure, $entityclassname = null)
    {

        $entity = $this->entityHelper->getEntityClass($structure, $entityclassname);

        return $entity;
    }


    /**
     * Replaces underscores + next char with camel cased next char
     *
     * global_thermonuclear_war -> globalThermonuclearWar
     *
     * @param   string $string
     *
     * @return  string
     */
    protected function underscoresToCamelCase($string)
    {
        $str = $this->structureHelper->underscoresToCamelCase($string);

        return $str;
    }


    /**
     * executes the setter for the given property
     *
     * @param   object $entity
     * @param   string $property
     * @param   mixed $value
     *
     * @return  object
     */
    protected function updateEntityProperty($entity, $property, $value)
    {

        $entity = $this->entityHelper->updateEntityProperty($entity, $property, $value);

        return $entity;
    }


    /**
     * checks if the given structure needs to be validated
     * if only the id is set to be saved the structure needs no validation
     *
     * @param   mixed $structure
     *
     * @return  boolean
     */
    protected function checkStructureNeedsValidation($structure)
    {

        $needsValidation = $this->structureValidator->needsValidation($structure);

        return $needsValidation;
    }


    /**
     * validate a structure based on its field annotations
     *
     * @param   mixed $structure
     *
     * @return  array $violations
     * @throws \Exception
     */
    public function validateStructure($structure)
    {

        $violations = $this->structureValidator->validate($structure);

        return $violations;
    }

}
