<?php
namespace Redhotmagma\ApiBundle\Service\Helper;

class StringHelper
{

    /**
     * Replaces underscores + next char with camel cased next char
     *
     * global_thermonuclear_war -> globalThermonuclearWar
     *
     * @param   string $string
     *
     * @return  string
     */
    public function underscoresToCamelCase($string)
    {

        $str = str_replace('_', '', ucwords($string, '_'));

        return $str;
    }
}
